<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class homecontroller extends Controller
{
    public function welcome(){
        return view('welcome');
    }
    
    public function index(){
        return view('index');
    }
    public function regist(){
        return view('register');
    }
    public function kirim(Request $request){
        $fnama =$request['fname'];
        $lnama =$request['lname'];
        $biodata = $request['bio'];
        $jenisKelamin = $request['jk'];
        return view('welcome', ['fname' => $fnama, 'lname' => $lnama, 'jenisKelamin' => $jenisKelamin , 'bio' => $biodata]);
    }


}
